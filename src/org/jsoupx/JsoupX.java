package org.jsoupx;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class JsoupX {

  public int timeOut = 999;
  private Connection conn;
  private Document doc;
  private static String url = null;

  private JsoupX(Connection conn) {
    this.conn = conn;
  }

  public static JsoupX connect(String url) {
    JsoupX.url = url;
    return new JsoupX(Jsoup.connect(url));
  }

  public JsoupX timeout(int time) {
    timeOut = time;
    return this;
  }

  public DocumentX get() throws IOException {
    doc = conn.timeout(timeOut).get();
    DocumentX d = new DocumentX(doc);
    d.all();
    return d;
  }
  
  public DocumentX post() throws IOException {
    doc = conn.timeout(timeOut).post();
    DocumentX d = new DocumentX(doc);
    d.all();
    return d;
  }

  public static DocumentX parse(String html) {
    return new DocumentX(Jsoup.parse(html));
  }
  
  public static DocumentX parse(Elements els) {
    return new DocumentX().parse(els);
  }

  public static String root(String url) {
    Pattern p =
        Pattern.compile("[a-zA-z]+://[^\\s'\"]*\\.(com|cn|net|org|biz|info|cc|tv)",
            Pattern.CASE_INSENSITIVE);
    Matcher matcher = p.matcher(url);
    if (matcher.find()) {
      String s = matcher.group();
      return s;
    }

    return null;
  }

  public static String domain(String url) {
    Pattern p =
        Pattern.compile("[^\\s'\"./:]*.(com|cn|net|org|biz|info|cc|tv)", Pattern.CASE_INSENSITIVE);
    try {
      Matcher matcher = p.matcher(url);
      if (matcher.find()) return matcher.group();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public static void main(String[] args) {
    String fileName =
        "http://tool.oschina.net//img/google_custom_search_watermark.gif".replaceAll("\\/\\/", "/");
    System.out.println(fileName);
  }


}
